#!/bin/bash
#
# Install Something on a Debian derivative
#

DEBIAN_FRONTEND=noninteractive
sudo apt-get update 
sudo apt-get install -yq --no-install-recommends \
     apt-transport-https \ # Just an example!
     httpie
sudo apt-get clean 
sudo rm -rf /var/lib/apt/lists/*
sudo apt-get autoremove -y

echo "My Cool App install succeeded" > /tmp/rapid_image_complete