#!/bin/bash
#
# spawn a child process to do the actual install, 
# and do it in a way that will not terminate even if the parent goes away
#
nohup ./do-the-install.sh > /tmp/rapid_image_status.txt 2> /tmp/rapid_image_status.err < /dev/null &