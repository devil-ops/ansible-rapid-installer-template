# Ansible Rapid Installer Template

Template Repo for an Ansible Installer in an Appstack in Rapid

## The "API" of an installer

These are cloned by the Rapid Installation process and the file named `install.sh` is executed. The file `/tmp/rapid_image_complete` is checked periodically. When this file exists the installer is considered completed.

1. Create a wrapper script that's going to be called by the Rapid install process and then fork.
  suggestion: `install.sh`
  example:

  ```bash
  #!/bin/bash
  #
  # spawn a child process to do the actual install, 
  # and do it in a way that will not terminate even if the parent goes away
  #
  nohup ./do-the-install.sh > /tmp/rapid_image_status.txt 2> /tmp/rapid_image_status.err < /dev/null &
  ```

2. Create another script that actually DOES the install
  suggestion: `do-the-install.sh`
  example:

  ```bash
  #!/bin/bash
  #
  # Install Something on a Debian derivative
  #

  DEBIAN_FRONTEND=noninteractive
  sudo apt-get update 
  sudo apt-get install -yq --no-install-recommends \
      apt-transport-https \ # Just an example!
      httpie
  sudo apt-get clean 
  sudo rm -rf /var/lib/apt/lists/*
  sudo apt-get autoremove -y

  echo "My Cool App install succeeded" > /tmp/rapid_image_complete
  ```

When done this way the Rapid install process will periodicaly check for the existence of `/tmp/rapid_image_complete` but doesn't need to stay connected continuously for the whole process. Logs will be under the `/tmp/rapid_image_status.{txt,err}` files for review should something go awry.

3. Create an [Ansible Installer in VCM](https://vcm.duke.edu/admin/installers) for this "App"
4. Add This installer to [an AppStack in VCM](https://vcm.duke.edu/admin/app_stacks)
   Alternatively, Create a new AppStack that references the Installer you created.

Other general guidelines:

- keep the project public and open.
- No secrets in the scripts.
- keep the project under the `devil-ops` group in gitlab to facilitate community contributions.
- documentation or per-user can be prepared as part of the installer if you wish and put into `/etc/skel` so that it ends up in the users homedirectory later.
